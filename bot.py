from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, BaseFilter, CallbackQueryHandler
from telegram import ReplyKeyboardMarkup, InlineKeyboardButton, InlineKeyboardMarkup, ChatAction
from config import TOKEN
import logging
import datetime

from MusicOperational import MusicOperational

def start(bot, update):
    bot.send_chat_action(chat_id=update.message.chat_id, action=ChatAction.TYPING)
    # custom_keyboard = [['/help'], ['Create a new graph']]
    # reply_markup = ReplyKeyboardMarkup(custom_keyboard)
    bot.send_message(chat_id=update.message.chat_id,
                     text="Дарова! Я - чертовски умный бот, который как боженька умеет искать музыку, "
                          "выкачивать её и отправлять по супер-защищённому каналу в телеге."
                          "\nПы.Сы. (Дисклеймер): То, что делает этот бот - нифига не по фэн-шую, лицензионному "
                          "соглашению и прочей важной законной фигне, используйте на свой страх и риск. "
                          "Создано исключительно в образовательных целях."
                          "\n\nFAQ: /find [название песни/группа] - найти песню."
                          "\nНапример: /find Кино - Группа крови")
    bot.send_chat_action(chat_id=update.message.chat_id, action=ChatAction.UPLOAD_VIDEO)
    bot.send_video(chat_id=update.message.chat_id, video=open('video.mp4', 'rb'))


def find(bot, update):
    bot.send_chat_action(chat_id=update.message.chat_id, action=ChatAction.UPLOAD_AUDIO)
    msg = update.message.text
    query = str(msg[5:]).strip()
    if len(query) < 4:
        update.message.reply_text('Слишком маленький запрос. Попробуйте ввести что-нибудь побольше')
        return

    reply = MusicOperational.get_track_info(query, 0)
    ref = MusicOperational.get_track_listen_url(query, 0)

    keyboard = [[InlineKeyboardButton("Слушать на soundcloud",
                                      url=ref if ref != 'none' else 'https://ru.wikipedia.org/wiki/HTTP_404'),
                 InlineKeyboardButton("Найти ещё", callback_data='next|||' + query + '|||' + str(1))]]

    reply_markup = InlineKeyboardMarkup(keyboard)

    download_file_location = str(update.message.chat_id) + \
                             str(update.message.from_user.id) + \
                             str(datetime.datetime.timestamp(datetime.datetime.now()))
    try:
        filename = MusicOperational.download_track(query, 0, download_file_location)
        print('Downloaded file to ' + filename)
        update.message.reply_audio(caption=reply, audio=open(filename, 'rb'), reply_markup=reply_markup)
    except:
        update.message.reply_text('Поиск и загрузка аудиозаписи не увенчались успехом...')

    MusicOperational.remove_folder(download_file_location)


def button(bot, update):
    callback = update.callback_query
    bot.send_chat_action(chat_id=callback.message.chat_id, action=ChatAction.UPLOAD_AUDIO)

    query_type, query, id = (str(i).strip() for i in str(callback.data).split('|||'))

    if query_type == 'next':
        iteration = int(id)
        reply = MusicOperational.get_track_info(query, iteration)
        ref = MusicOperational.get_track_listen_url(query, iteration)

        keyboard = [[InlineKeyboardButton("Слушать на soundcloud",
                                          url=ref if ref != 'none' else 'https://ru.wikipedia.org/wiki/HTTP_404'),
                     InlineKeyboardButton("Найти ещё", callback_data='next|||' + query + '|||' + str(iteration + 1))]]

        reply_markup = InlineKeyboardMarkup(keyboard)

        download_file_location = str(callback.message.chat_id) + \
                                 str(callback.message.from_user.id) + \
                                 str(datetime.datetime.timestamp(datetime.datetime.now()))
        try:
            filename = MusicOperational.download_track(query, iteration, download_file_location)
            print('Downloaded file to ' + filename)
            callback.message.reply_audio(caption=reply, audio=open(filename, 'rb'), reply_markup=reply_markup)
        except:
            callback.message.reply_text('Больше аудиозаписей найти не удалось...')

        MusicOperational.remove_folder(download_file_location)


def main():
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

    updater = Updater(TOKEN)
    dp = updater.dispatcher
    print("Initialising dispatcher")
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", start))
    dp.add_handler(CommandHandler("find", find))

    updater.dispatcher.add_handler(CallbackQueryHandler(button))
    print("Handlers ready")
    updater.start_polling()
    print("Starting polling")
    updater.idle()


if __name__ == '__main__':
    main()
