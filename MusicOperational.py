import os
import shutil
from soundscrape.soundscrape import process_soundcloud
import soundcloud
from os import listdir
from os.path import isfile, join


class MusicOperational:

    @staticmethod
    def remove_folder(folder_name):
        if os.path.exists(folder_name):
            shutil.rmtree(folder_name)

    @staticmethod
    def convert_duration(duration):
        duration = int(duration) // 1000
        hours = duration // 3600
        duration %= 3600
        mins = duration // 60
        duration %= 60
        secs = duration

        ans = ''
        if hours > 0:
            ans += str(hours) + ':'
        ans += str(mins).zfill(2) + ':' + str(secs).zfill(2)
        return ans

    @staticmethod
    def convert_title(title):
        if len(title) <= 77:
            return str(title)
        return title[:77] + '...'

    @staticmethod
    def get_track_list(author_name, track_name):
        client = soundcloud.Client(client_id='175c043157ffae2c6d5fed16c3d95a4c')
        tracks = client.get('/tracks', q=str(author_name) + '-' + str(track_name))

        ans = []
        for i in range(min(10, len(tracks))):
            ans += [str(i + 1) + ') ' + MusicOperational.convert_title(tracks[i].title) \
                    + '\n    Длительность: ' + MusicOperational.convert_duration(tracks[i].duration) \
                    + '\n    Ссылка для прослушивания: ' + str(tracks[i].permalink_url) + '\n\n']

        return ans

    @staticmethod
    def download_track(query, track_number, folder_name='track'):
        MusicOperational.remove_folder(folder_name)
        os.makedirs(folder_name)

        client = soundcloud.Client(client_id='175c043157ffae2c6d5fed16c3d95a4c')
        tracks = client.get('/tracks', q=query)

        if track_number >= len(tracks):
            return False

        url = tracks[track_number].permalink_url
        vargs = {'path': folder_name, 'folders': False, 'group': False, 'track': '', 'num_tracks': 1, 'bandcamp': False,
                 'downloadable': False, 'likes': False, 'open': False,
                 'artist_url': url, 'keep': False}
        process_soundcloud(vargs)

        files_downloaded = [f for f in listdir(folder_name) if isfile(join(folder_name, f))]
        if len(files_downloaded) > 0:
            return folder_name + '/' + str(files_downloaded[0])
        else:
            return 'none'

    @staticmethod
    def get_track_info(query, track_number=0):
        client = soundcloud.Client(client_id='175c043157ffae2c6d5fed16c3d95a4c')
        tracks = client.get('/tracks', q=query)

        if track_number >= len(tracks):
            return 'Больше подходящих треков не найдено :('

        return str(track_number + 1) + ') ' + MusicOperational.convert_title(tracks[track_number].title) \
               + '\nДлительность: ' + MusicOperational.convert_duration(tracks[track_number].duration)
    @staticmethod
    def get_track_listen_url(query, track_number=0):
        client = soundcloud.Client(client_id='175c043157ffae2c6d5fed16c3d95a4c')
        tracks = client.get('/tracks', q=query)
        if track_number >= len(tracks):
            return 'none'

        return tracks[track_number].permalink_url